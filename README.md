# The Word & Spirit App
*A Single-Page Web App for Bookshop/Lending Library.*

## Contents:

[Intro](#intro). [Mission](#mission). [Stack](#stack). [Contributing](#contributing). [License](#license). [Documentation](#documentation).

## Intro.

This was started by [Peter Prescott](https://twitter.com/peter_prescott), Sam Tomlin and John Bruce, when [Peter's idea for a bookshop](https://peter.prescott.org.uk/footnotes/relevant-bits/) came together with John Bruce's vision for a [media library](https://peter.prescott.org.uk/footnotes/bruce-on-books/) and the availability of a spare room at the [Salvation Army Church, Stoneycroft](https://oldswanchurch.org.uk).


## Mission

In general, as a ministry of [the Salvation Army](https://www.salvationarmy.org.uk/our-mission-vision-and-values):

- Save souls
- Grow saints
- Serve suffering humanity.

In particular, as a bookshop/lending-library:
*By the cunning use of books and technology*.

## Issues

There are sure to be all sorts of things that need fixing for this to work properly. And in the very process of fixing some things we will break others. Ideally, everyone would be able and feel confident to [report all issues on Gitlab](https://gitlab.com/wordandspirit/app/issues), so that they can be openly engaged with and fixed by anyone with the time. But if you don't feel like you can do that, just [send me a message](https://chat.whatsapp.com/FaA3BHaBjs64YiMkuPMu0a).

## Stack

Front-End: HTML/CSS/[Vanilla JS](http://vanilla-js.com/)

Back-End: [Python](https://www.python.org/) ([Flask](https://www.palletsprojects.com/p/flask/))

Database: [SQLite3](https://www.sqlite.org/about.html)


## Contributing

Please help develop this.

You will need an internet connection and a computer (it doesn't have to be fancy).

Then open your [command-line](https://en.wikipedia.org/wiki/Command-line_interface): [Powershell](https://en.wikipedia.org/wiki/PowerShell) if you're on Windows, otherwise [Bash](https://en.wikipedia.org/wiki/Bash_(Unix_shell)).

You need to have installed [Git](https://git-scm.com/) (for version control) and [Python](https://www.python.org/). Check if they are by typing these lines of code into Powershell: `git version`, `python --version`.

I'll preface the commands I want you to type with two arrows: **>>** -- but don't type the arrows. And then I'll show you the first few lines of what came up on my system -- you might get slightly different versions. 

```
>> git version
git version 2.22.0.windows.1

>> python --version
Python 3.7.4
```

If you haven't done this sort of thing before they probably aren't. Not to worry!

The easiest way to get them properly installed is to use [Choco](https://chocolatey.org/install), which makes installing things as easy as typing two words. But we do have to install that first... Make sure you're running Powershell as an Administrator (right-click on the Powershell icon and select **Run as Administrator**), and then copy this command:
`Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))`

```
>>> Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

Getting latest version of the Chocolatey package for download.
Getting Chocolatey from https://chocolatey.org/api/v2/package/chocolatey/0.10.15.
Extracting C:\Users\presc\AppData\Local\Temp\chocolatey\chocInstall\chocolatey.zip to C:\Users\presc\AppData\Local\Temp\chocolatey\chocInstall...
```

You should then see the system download and install Choco for you. Check that it has worked: `choco`.

```
>>> choco

Chocolatey v0.10.15
Please run 'choco -?' or 'choco <command> -?' for help menu.
```
<a id="env_variables"></a>
(If it hasn't, you may need to select **Edit the system environment variables** from the Start Menu, then click **Environment Variables**, select **Path** from the **System Variables**, click **Edit**, and then add as **New** the name of the folder where Choco has been saved -- which will be something like this: `C:\ProgramData\chocolatey\bin`).

Once Choco is installed, you can easily install git and python: 
```
>>> choco install git -y
Installing the following packages:
git
By installing you accept licenses for the packages...

>>> choco install python -y
Installing the following packages:
python
By installing you accept licenses for the packages...
```

And to really make the most of the power of Git, you need to sign up for a free web-based git repository hosting service like [Github](https://github.com) or [Gitlab](https://gitlab.com/users/sign_up). Let's go with the latter, since they're an amazing example of a [company culture](https://about.gitlab.com/company/culture/) that is committed to the open-source philosophy that **Everyone can contribute**.

Once you have [signed up for a Gitlab account at Gitlab.com](https://gitlab.com/users/sign_up), go to the [project page for this app: https://gitlab.com/wordandspirit/app](https://gitlab.com/wordandspirit/app), and click **Fork** (on the right, about a third of the way down the page). Then click on your Account Name, and you will have your own copy of the code for this project -- to play with as you like, without fear of breaking anything!

Back in your command line, you can now clone that code on to your computer (note that instead of `new account`, you need to write your actual account name):
```
git clone https://gitlab.com/newaccount/app.git
```

Now you can navigate into the `app` folder, install [`pipenv`](pipenv) to manage your dependencies, and get started.
```
cd app
pip install pipenv
pipenv install
pipenv shell
python run.py
```

If that all worked, the final command should have run the `run.py` file, which should have both opened up the Single-Page Web-App interface in your browser (the 'front-end), and continued running a local development server ('the back-end) which will engage with commands from the browser, like 'Search for this phrase', or 'Sign up a new user', or 'Log in an existing user'.

Anyway, that is probably enough to get started. If you're completely new to coding, then you can start learning for free at [freecodecamp.org](https://www.freecodecamp.org/learn). If you have some experience, then you will easily be able to help point out things that I should be doing that I'm not.

## License.

The project is [Free Open-Source Software](https://en.wikipedia.org/wiki/Free_and_open-source_software), and is licensed under [the most popular](https://www.slant.co/topics/1141/~open-source-licenses) open-source license: [the MIT license](https://opensource.org/licenses/MIT):

> Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so...

## Documentation.

TODO: Set up auto-documentation with Sphinx.
